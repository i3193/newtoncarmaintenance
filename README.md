# ISAAC Software Solutions B.V.

### Assignment for PHP Developer position
* Versie: 1.0
* Datum: 4-11-2021
* Pagina 2 van 3

### Context
A car maintenance company “Newton Car Maintenance” wants to register the maintenance jobs it
does on various cars, for various customers. Some maintenance jobs are specific for a certain brand of
car, or even a specific model of a car brand, others apply to all types of cars of all cars of a
brand/model. Each maintenance job has a fixed rate in terms of service hours and may require certain
spare parts. Spare parts can also be generic for (almost) all cars, others are specific per brand or even
per model. E.g., many cars share the same engine block.
A software architect made the following simplified class diagram. Properties and operations are
omitted.

![img.png](docs/class_diagram.png)

### Assignment
According to the class diagram, write some modern PHP-code that calculates the total
price for servicing a car (i.e., the price for an instance of ScheduledMainteanceJob). You
may not need to implement all of the classes in the diagram and you are allowed to add
extra classes if you think that’s necessary. Use object-oriented methodologies and use
some of the latest PHP features. Make sure your code runs on the latest version of PHP.
Your calculation should keep track of the following aspects:
* Maintenance servicing hours performed in the weekend costs more than maintenance during
weekdays
* Spare parts have different costs based on the type of part
* Each type of maintenance job has a fixed rate in terms of service hours
* Sum up spare part prices and servicing hours
* Calculate VAT

Now, also write some code that gets the actual, current price of spare parts from an
external system at the car manufacturer. The price should be fetched in real-time and
used in the total servicing price calculation implemented above.
Make a list of all assumptions you made that were required for completing this
assignment.

Note: Use a reasonable level of comments in your code so others will be able to maintain
it.

 
## Assumptions

Due to the same name convention for Model in Laravel, I assumed it was alright to change the classname to **carModel**

Assumtions for the calculation are:
* price per hour = 75 / 80 / 85 euro
* VAT = 21%

For the  Rates and Vat I created new tables, and linked them to the correct models.

For the dynamic prices, if assume, that I can get the correct price from the first
Brand, if the sparePart has any.
If not, than i will use the fixedPrice.

I created a mock api enpoint for the dynamic prices.  
http://newtoncarmaintenance.test/api/v1/price. 
This API enpoint just returns a random price, the data send with the post is not used.

## Getting started
### Install

Changed the .env file with teh proper location of the sqlite database.
* {YOUR_PATH}/NewtonCarMaintenance/database/newton_car_maintenance.sqlite

Run the following commands to create a set of data.
```composer log
php artisan migration:fresh
```
```composer log
php artisan db:seed
```

### Commands

Use the following command to get the requested result. Replace {ID} with an existing ID.
The seeder has 100 ids prepared
```composer log
php artisan newton:calculate {ID}
```
For example
```composer log
php artisan newton:calculate 1
```

### Example Result  
![img.png](docs/output-result.png)


