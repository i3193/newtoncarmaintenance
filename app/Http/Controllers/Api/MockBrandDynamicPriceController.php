<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Faker\Generator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class MockBrandDynamicPriceController extends Controller
{
    /**
     * @param Request $request
     * @param $brand
     * @return JsonResponse
     * @throws ValidationException
     */
    public function index(
        Request $request,
        Generator $faker
    )
    {
        $validator = Validator::make($request->all(), [
            'brand' => 'required',
            'partNumber' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $validatedData = $validator->validated();

        return response()->json([
            'price' => $faker->randomFloat(2, 0, 1200)
        ]);
    }
}
