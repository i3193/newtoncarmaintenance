<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaintenanceJob extends Model
{
    use HasFactory;

    public function spareParts()
    {
        return $this->belongsToMany(SparePart::class,'maintenance_job_spare_parts');
    }

    public function scheduledMaintenanceTasks()
    {
        return $this->belongsToMany(ScheduledMaintenanceJob::class);
    }

    public function hourRate()
    {
        return $this->belongsTo(HourRates::class);
    }
}
