<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarModel extends Model
{
    use HasFactory;

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function spareParts()
    {
        return $this->belongsToMany(SparePart::class, 'spare_part_car_models');
    }

    public function cars()
    {
        return $this->belongsToMany(Car::class, 'car_model_cars');
    }
}
