<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    public function scheduledMaintenanceTasks()
    {
        return $this->belongsToMany(ScheduledMaintenanceJob::class, 'scheduled_maintenance_job_cars');
    }

    public function brand()
    {
        return $this->hasOneThrough(Brand::class, CarModel::class);
    }

    public function carModel()
    {
        return $this->belongsTo(CarModel::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
