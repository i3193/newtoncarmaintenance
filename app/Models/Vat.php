<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vat extends Model
{
    public function scheduledMaintenanceTasks()
    {
        return $this->belongsToMany(ScheduledMaintenanceJob::class);
    }
}
