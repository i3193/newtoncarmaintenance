<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    public function spareParts()
    {
        return $this->belongsToMany(SparePart::class, 'spare_part_brands');
    }

    public function carModels()
    {
        return $this->belongsToMany(CarModel::class, 'brand_car_model');
    }
}
