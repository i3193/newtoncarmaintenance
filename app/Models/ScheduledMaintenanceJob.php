<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduledMaintenanceJob extends Model
{
    use HasFactory;

    public function timeSlot()
    {
        return $this->belongsTo(TimeSlot::class);
    }

    public function spareParts()
    {
        return $this->hasManyThrough(SparePart::class, MaintenanceJob::class);
    }

    public function maintenanceJob()
    {
        return $this->belongsTo(MaintenanceJob::class);
    }

    public function cars()
    {
        return $this->belongsToMany(Car::class, 'scheduled_maintenance_job_cars');
    }

    public function customer()
    {
        return $this->hasOneThrough(Customer::class, Car::class);
    }

    public function engineer()
    {
        return $this->belongsTo(Engineer::class);
    }

    public function vat()
    {
        return $this->belongsTo(Vat::class);
    }

}
