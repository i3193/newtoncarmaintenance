<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SparePart extends Model
{
    use HasFactory;

    public float $dynamicPrice;


    public function brands()
    {
        return $this->belongsToMany(Brand::class, 'spare_part_brands');
    }

    public function carModels()
    {
        return $this->belongsToMany(CarModel::class, 'spare_part_car_models');
    }

    public function maintenanceJobs()
    {
        return $this->belongsToMany(MaintenanceJob::class, 'maintenance_job_spare_parts');
    }
}
