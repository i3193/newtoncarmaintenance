<?php

namespace App\Models\Jobs\Vat;

use App\Models\Vat;

class VatPercentage
{
    public function get(Vat $vat):int
    {
       return intval($vat->percentage);
    }
}
