<?php

namespace App\Models\Jobs\HourRate;

use App\Models\HourRates;
use Carbon\Carbon;

class HourRateByCarbon
{
    public function get(HourRates $hourRate,Carbon $date)
    {
        if ($date->isWeekend()) {
            return $hourRate->weekend;
        }
        return $hourRate->regular;
    }
}
