<?php

namespace App\Models\Jobs\ScheduledMaintenanceJob;

use App\Models\ScheduledMaintenanceJob;

class ScheduledMaintenanceJobsById
{
    public function get(int $id)
    {
        return ScheduledMaintenanceJob::find($id);
    }
}
