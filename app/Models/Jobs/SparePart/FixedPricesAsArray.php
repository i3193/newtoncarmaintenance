<?php

namespace App\Models\Jobs\SparePart;

use App\Bundle\DynamicPrices\DynamicPricesByJson;
use App\Models\Brand;
use App\Models\SparePart;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Client\Response;

class FixedPricesAsArray
{
    public function get(Collection $spareParts)
    {
        return $spareParts->pluck('fixedPrice')->toArray();
    }
}
