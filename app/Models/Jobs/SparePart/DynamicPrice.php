<?php

namespace App\Models\Jobs\SparePart;

use App\Bundle\DynamicPrices\DynamicPricesByJson;
use App\Models\Brand;
use App\Models\SparePart;
use Illuminate\Http\Client\Response;

class DynamicPrice
{
    public function get(SparePart $sparePart)
    {
        $brand = $sparePart->brands->first();
        $dynamicPrice = $sparePart->fixedPrice;
        if (
            $brand instanceof Brand
            && !empty($brand->priceApiUrl)
            && !empty($brand->priceApiKey)
        ) {
            /** @var Response $response */
            $response = (new DynamicPricesByJson($brand->priceApiUrl, $brand->priceApiKey))->json([
                'brand' => $brand->name,
                'partNumber' => $sparePart->partNumber
            ]);
            if ($response->ok()) {
                $dynamicPrice = $response->json('price');
            }
        }
        return $dynamicPrice;
    }
}
