<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\Collection;

class ScheduledMaintenanceJobResult implements Arrayable, Jsonable
{
    private string $maintenanceName = '';
    private string $maintenanceType;
    private string $engineerName;
    private int $vatPercentage;
    private Carbon $maintenanceStart;
    private Collection $cars;
    private Collection $spareParts;
    private float $sparePartFixedPriceTotalExclVat;
    private float $sparePartDynamicPriceTotalExclVat;
    private float $calculatedServiceHours;
    private float $actualServiceHours;
    private float $serviceHourRate;
    private float $serviceHoursTotalPriceExclVat;
    private float $serviceHoursTotalPriceVat;
    private float $serviceHoursTotalPriceInclVat;
    private float $subTotalExclVat;
    private float $subTotalVat;
    private float $totalInclVat;
    private float $sparePartPriceTotalVat;
    private float $sparePartPriceTotalInclVat;

    public function toArray()
    {
        return [
            'maintenanceName' => $this->maintenanceName,
            'maintenanceType' => $this->maintenanceType,
            'engineerName' => $this->engineerName,
            'vatPercentage' => $this->vatPercentage,
            'maintenanceStart' => $this->maintenanceStart,
            'spareParts' => $this->spareParts,
            'cars' => $this->cars,
            'sparePartFixedPriceTotalExclVat' => $this->sparePartFixedPriceTotalExclVat,
            'sparePartDynamicPriceTotalExclVat' => $this->sparePartDynamicPriceTotalExclVat,
            'sparePartPriceTotalVat' => $this->sparePartPriceTotalVat,
            'sparePartPriceTotalInclVat' => $this->sparePartPriceTotalInclVat,
            'calculatedServiceHours' => $this->calculatedServiceHours,
            'actualServiceHours' => $this->actualServiceHours,
            'serviceHourRate' => $this->serviceHourRate,
            'serviceHoursTotalPriceExclVat' => $this->serviceHoursTotalPriceExclVat,
            'serviceHoursTotalPriceVat' => $this->serviceHoursTotalPriceVat,
            'serviceHoursTotalPriceInclVat' => $this->serviceHoursTotalPriceInclVat,
            'subTotalExclVat' => $this->subTotalExclVat,
            'subTotalVat' => $this->subTotalVat,
            'totalInclVat' => $this->totalInclVat,
        ];
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }

    /**
     * @return string
     */
    public function getMaintenanceName(): string
    {
        return $this->maintenanceName;
    }

    /**
     * @param string $maintenanceName
     * @return ScheduledMaintenanceJobResult
     */
    public function setMaintenanceName(string $maintenanceName): ScheduledMaintenanceJobResult
    {
        $this->maintenanceName = $maintenanceName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaintenanceType(): string
    {
        return $this->maintenanceType;
    }

    /**
     * @param string $maintenanceType
     * @return ScheduledMaintenanceJobResult
     */
    public function setMaintenanceType(string $maintenanceType): ScheduledMaintenanceJobResult
    {
        $this->maintenanceType = $maintenanceType;
        return $this;
    }

    /**
     * @return string
     */
    public function getEngineerName(): string
    {
        return $this->engineerName;
    }

    /**
     * @param string $engineerName
     * @return ScheduledMaintenanceJobResult
     */
    public function setEngineerName(string $engineerName): ScheduledMaintenanceJobResult
    {
        $this->engineerName = $engineerName;
        return $this;
    }


    /**
     * @return int
     */
    public function getVatPercentage(): int
    {
        return $this->vatPercentage;
    }

    /**
     * @param int $vatPercentage
     * @return ScheduledMaintenanceJobResult
     */
    public function setVatPercentage(int $vatPercentage): ScheduledMaintenanceJobResult
    {
        $this->vatPercentage = $vatPercentage;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getMaintenanceStart(): Carbon
    {
        return $this->maintenanceStart;
    }

    /**
     * @param Carbon $maintenanceStart
     * @return ScheduledMaintenanceJobResult
     */
    public function setMaintenanceStart(Carbon $maintenanceStart): ScheduledMaintenanceJobResult
    {
        $this->maintenanceStart = $maintenanceStart;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getSpareParts(): Collection
    {
        return $this->spareParts;
    }

    /**
     * @param Collection $spareParts
     * @return ScheduledMaintenanceJobResult
     */
    public function setSpareParts(Collection $spareParts): ScheduledMaintenanceJobResult
    {
        $this->spareParts = $spareParts;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    /**
     * @param Collection $cars
     * @return ScheduledMaintenanceJobResult
     */
    public function setCars(Collection $cars): ScheduledMaintenanceJobResult
    {
        $this->cars = $cars;
        return $this;
    }

    /**
     * @return float
     */
    public function getSparePartFixedPriceTotalExclVat(): float
    {
        return $this->sparePartFixedPriceTotalExclVat;
    }

    /**
     * @param float $sparePartFixedPriceTotalExclVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setSparePartFixedPriceTotalExclVat(float $sparePartFixedPriceTotalExclVat): ScheduledMaintenanceJobResult
    {
        $this->sparePartFixedPriceTotalExclVat = $sparePartFixedPriceTotalExclVat;
        return $this;
    }

    /**
     * @return float
     */
    public function getSparePartDynamicPriceTotalExclVat(): float
    {
        return $this->sparePartDynamicPriceTotalExclVat;
    }

    /**
     * @param float $sparePartDynamicPriceTotalExclVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setSparePartDynamicPriceTotalExclVat(float $sparePartDynamicPriceTotalExclVat): ScheduledMaintenanceJobResult
    {
        $this->sparePartDynamicPriceTotalExclVat = $sparePartDynamicPriceTotalExclVat;
        return $this;
    }

    /**
     * @return float
     */
    public function getSparePartPriceTotalVat(): float
    {
        return $this->sparePartPriceTotalVat;
    }

    /**
     * @param float $sparePartPriceTotalVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setSparePartPriceTotalVat(float $sparePartPriceTotalVat): ScheduledMaintenanceJobResult
    {
        $this->sparePartPriceTotalVat = $sparePartPriceTotalVat;
        return $this;
    }

    /**
     * @return float
     */
    public function getSparePartPriceTotalInclVat(): float
    {
        return $this->sparePartPriceTotalInclVat;
    }

    /**
     * @param float $sparePartPriceTotalInclVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setSparePartPriceTotalInclVat(float $sparePartPriceTotalInclVat): ScheduledMaintenanceJobResult
    {
        $this->sparePartPriceTotalInclVat = $sparePartPriceTotalInclVat;
        return $this;
    }



    /**
     * @return float
     */
    public function getCalculatedServiceHours(): float
    {
        return $this->calculatedServiceHours;
    }

    /**
     * @param float $calculatedServiceHours
     * @return ScheduledMaintenanceJobResult
     */
    public function setCalculatedServiceHours(float $calculatedServiceHours): ScheduledMaintenanceJobResult
    {
        $this->calculatedServiceHours = $calculatedServiceHours;
        return $this;
    }

    /**
     * @return float
     */
    public function getActualServiceHours(): float
    {
        return $this->actualServiceHours;
    }

    /**
     * @param float $actualServiceHours
     * @return ScheduledMaintenanceJobResult
     */
    public function setActualServiceHours(float $actualServiceHours): ScheduledMaintenanceJobResult
    {
        $this->actualServiceHours = $actualServiceHours;
        return $this;
    }

    /**
     * @return float
     */
    public function getServiceHourRate(): float
    {
        return $this->serviceHourRate;
    }

    /**
     * @param float $serviceHourRate
     * @return ScheduledMaintenanceJobResult
     */
    public function setServiceHourRate(float $serviceHourRate): ScheduledMaintenanceJobResult
    {
        $this->serviceHourRate = $serviceHourRate;
        return $this;
    }

    /**
     * @return float
     */
    public function getServiceHoursTotalPriceExclVat(): float
    {
        return $this->serviceHoursTotalPriceExclVat;
    }

    /**
     * @param float $serviceHoursTotalPriceExclVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setServiceHoursTotalPriceExclVat(float $serviceHoursTotalPriceExclVat): ScheduledMaintenanceJobResult
    {
        $this->serviceHoursTotalPriceExclVat = $serviceHoursTotalPriceExclVat;
        return $this;
    }

    /**
     * @return float
     */
    public function getServiceHoursTotalPriceVat(): float
    {
        return $this->serviceHoursTotalPriceVat;
    }

    /**
     * @param float $serviceHoursTotalPriceVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setServiceHoursTotalPriceVat(float $serviceHoursTotalPriceVat): ScheduledMaintenanceJobResult
    {
        $this->serviceHoursTotalPriceVat = $serviceHoursTotalPriceVat;
        return $this;
    }

    /**
     * @return float
     */
    public function getServiceHoursTotalPriceInclVat(): float
    {
        return $this->serviceHoursTotalPriceInclVat;
    }

    /**
     * @param float $serviceHoursTotalPriceInclVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setServiceHoursTotalPriceInclVat(float $serviceHoursTotalPriceInclVat): ScheduledMaintenanceJobResult
    {
        $this->serviceHoursTotalPriceInclVat = $serviceHoursTotalPriceInclVat;
        return $this;
    }



    /**
     * @return float
     */
    public function getSubTotalExclVat(): float
    {
        return $this->subTotalExclVat;
    }

    /**
     * @param float $subTotalExclVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setSubTotalExclVat(float $subTotalExclVat): ScheduledMaintenanceJobResult
    {
        $this->subTotalExclVat = $subTotalExclVat;
        return $this;
    }

    /**
     * @return float
     */
    public function getSubTotalVat(): float
    {
        return $this->subTotalVat;
    }

    /**
     * @param float $subTotalVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setSubTotalVat(float $subTotalVat): ScheduledMaintenanceJobResult
    {
        $this->subTotalVat = $subTotalVat;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalInclVat(): float
    {
        return $this->totalInclVat;
    }

    /**
     * @param float $totalInclVat
     * @return ScheduledMaintenanceJobResult
     */
    public function setTotalInclVat(float $totalInclVat): ScheduledMaintenanceJobResult
    {
        $this->totalInclVat = $totalInclVat;
        return $this;
    }


}
