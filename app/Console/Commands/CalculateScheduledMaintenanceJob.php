<?php

namespace App\Console\Commands;

use App\Feature\ScheduledMaintenanceAggregatedOutput;
use App\Feature\ScheduledMaintenanceJobCalculations;
use App\Models\ScheduledMaintenanceJob;
use App\Models\ScheduledMaintenanceJobResult;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class CalculateScheduledMaintenanceJob extends Command
{

    private ScheduledMaintenanceAggregatedOutput $aggeratedOutput;
    private ScheduledMaintenanceJobCalculations $scheduledMaintenanceJobCalculations;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newton:calculate
                            {id : Id of Scheduled Maintenance Job}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For calculating the Total price of a Scheduled Maintenance Job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ScheduledMaintenanceAggregatedOutput $aggeratedOutput,
        ScheduledMaintenanceJobCalculations $scheduledMaintenanceJobCalculations
    )
    {
        parent::__construct();
        $this->aggeratedOutput = $aggeratedOutput;
        $this->scheduledMaintenanceJobCalculations = $scheduledMaintenanceJobCalculations;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $id = $this->argument('id');
        $strPaddingRight = 40;

        /** @var ScheduledMaintenanceJobResult $result */
        $result = $this->scheduledMaintenanceJobCalculations->calculate($id);

        if ($result instanceof ScheduledMaintenanceJobResult
            && !empty($result->getMaintenanceName())) {

            $vat = $result->getVatPercentage();
            $carsTableResult = $this->aggeratedOutput->getCarsAsConsoleTableArray($result->getCars());
            $sparePartTableResult = $this->aggeratedOutput->getSparePartsAsConsoleTableArray($result->getSpareParts());

            $this->output->title('Newton Car Maintenance');
            $this->output->section('Scheduled maintenance job price calculator');

            $this->output->section('General information');
            $this->output->writeln(sprintf("%s %s",
                    str_pad('Maintenance name :', $strPaddingRight),
                    $result->getMaintenanceName())
            );
            $this->output->writeln(sprintf("%s %s",
                    str_pad('Maintenance type :', $strPaddingRight),
                    $result->getMaintenanceType())
            );
            $this->output->writeln(sprintf("%s %s",
                str_pad('Maintenance start at :', $strPaddingRight),
                    $result->getMaintenanceStart()->format('d-m-Y @ H:i'))
            );
            $this->output->writeln(sprintf("%s %s",
                str_pad('Engineer :', $strPaddingRight),
                    $result->getEngineerName())
            );
            $this->output->newLine();
            $this->output->writeln('Registered Cars for Maintenance :');
            $this->output->table(
                ['Id', 'VIN', 'License plate', 'Brand', 'Model', 'Customer', 'Email'],
                $carsTableResult
            );

            $this->output->newLine();
            $this->output->section('Spare Parts List (excl.'. $vat .'% VAT )');
            $this->output->table(
                ['Id', 'Name', 'ProductCode', 'Brand', 'Model', 'FixedPrice', 'DynamicPrice'],
                $sparePartTableResult
            );

            $this->output->section('Spare Parts totals');
            $this->output->writeln(sprintf("%s %s",
                str_pad('Subtotal Fixed price (excl.'. $vat .'% VAT ) : ', $strPaddingRight),
                    $result->getSparePartFixedPriceTotalExclVat())
            );

            $this->output->newLine();
            $this->output->writeln(sprintf("%s %s",
                    str_pad('Subtotal price (excl.'. $vat .'% VAT ) : ', $strPaddingRight),
                    $result->getSparePartDynamicPriceTotalExclVat()
                )
            );
            $this->output->writeln(sprintf("%s %s",
                str_pad(''. $vat .'% VAT : ', $strPaddingRight),
                $result->getSparePartPriceTotalVat()
                )
            );
            $this->output->writeln(sprintf("%s %s",
                str_pad('Total (incl.'. $vat .'% VAT ) : ', $strPaddingRight),
                    $result->getSparePartPriceTotalInclVat()
                )
            );


            $this->output->section('Service hours');
            $this->output->writeln(sprintf("%s %s",
                str_pad('Calculated Service hours : ', $strPaddingRight),
                $result->getCalculatedServiceHours()
                )
            );
            $this->output->writeln(sprintf("%s %s",
                str_pad('Real Service hours : ', $strPaddingRight),
                    $result->getActualServiceHours()
                )
            );
            $this->output->writeln(sprintf("%s %s",
                    str_pad( 'Service hours rate (based on date) :', $strPaddingRight),
                    $result->getServiceHourRate()
                )
            );
            $this->output->newLine();
            if($result->getMaintenanceStart()->isWeekend()){
                $this->output->note('Using Weekend rates');
            }
            $this->output->writeln(sprintf("%s %s",
                    str_pad('SubTotal (excl.'. $vat .'% VAT ) :', $strPaddingRight),
                    $result->getServiceHoursTotalPriceExclVat()
                )
            );
            $this->output->writeln(sprintf("%s %s",
                    str_pad('' . $vat .'% VAT :', $strPaddingRight),
                    $result->getServiceHoursTotalPriceVat()
                )
            );
            $this->output->writeln(sprintf("%s %s",
                    str_pad('Total (incl.'. $vat .'% VAT ) :', $strPaddingRight),
                    $result->getServiceHoursTotalPriceInclVat()
                )
            );

            $this->output->newLine(2);
            $this->output->section('Totals');
            $this->output->writeln(sprintf("%s %s",
                    str_pad('SubTotal (excl.'. $vat .'% VAT ) :', $strPaddingRight),
                    $result->getSubTotalExclVat()
                )
            );
            $this->output->writeln(sprintf("%s %s",
                    str_pad($vat .'% VAT :', $strPaddingRight),
                    $result->getSubTotalVat()
                )
            );
            $this->output->writeln(sprintf("%s %s",
                    str_pad('Total (incl.'. $vat .'% VAT ) :', $strPaddingRight),
                    $result->getTotalInclVat()
                )
            );
            $this->output->success(['Thank you for calculating the Scheduled Maintenance Job.', 'Have a Nice day.']);
        } else {
            $this->output->error('No valid ScheduledMaintenanceJob found.');
        }
        return 0;
    }

}
