<?php

namespace App\Bundle\DynamicPrices;

use Illuminate\Support\Facades\Http;

class DynamicPricesByJson extends Client
{
    public function json(array $data)
    {
        return Http::withHeaders(['X-HTTP-API-KEY' => $this->apiKey])->post($this->apiUrl, $data);
    }

}
