<?php

namespace App\Bundle\DynamicPrices;

abstract class Client
{

    protected string $apiUrl;

    protected string $apiKey;

    public function __construct(string $apiUrl, string $apiKey)
    {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
        return $this;
    }

    abstract public function json(array $data);
}
