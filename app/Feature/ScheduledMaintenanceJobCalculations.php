<?php

namespace App\Feature;

use App\Bundle\DynamicPrices\DynamicPricesByJson;
use App\Models\Brand;
use App\Models\Jobs\HourRate\HourRateByCarbon;
use App\Models\Jobs\ScheduledMaintenanceJob\ScheduledMaintenanceJobsById;
use App\Models\Jobs\SparePart\DynamicPrice;
use App\Models\Jobs\SparePart\FixedPricesAsArray;
use App\Models\Jobs\Vat\VatPercentage;
use App\Models\ScheduledMaintenanceJob;
use App\Models\ScheduledMaintenanceJobResult;
use Carbon\Carbon;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class ScheduledMaintenanceJobCalculations
{
    private ScheduledMaintenanceJobsById $scheduledMaintenanceJobsById;
    private VatPercentage $vatPercentage;
    private DynamicPrice $dynamicPrice;
    private FixedPricesAsArray $fixedPricesAsArray;
    private HourRateByCarbon $hourRateByCarbon;

    /**
     * @param ScheduledMaintenanceJobsById $scheduledMaintenanceJobsById
     * @param VatPercentage $vatPercentage
     * @param DynamicPrice $dynamicPrice
     * @param FixedPricesAsArray $fixedPricesAsArray
     * @param HourRateByCarbon $hourRateByCarbon
     */
    public function __construct(
        ScheduledMaintenanceJobsById $scheduledMaintenanceJobsById,
        VatPercentage                $vatPercentage,
        DynamicPrice                 $dynamicPrice,
        FixedPricesAsArray           $fixedPricesAsArray,
        HourRateByCarbon             $hourRateByCarbon
    )
    {
        $this->scheduledMaintenanceJobsById = $scheduledMaintenanceJobsById;
        $this->vatPercentage = $vatPercentage;
        $this->dynamicPrice = $dynamicPrice;
        $this->fixedPricesAsArray = $fixedPricesAsArray;
        $this->hourRateByCarbon = $hourRateByCarbon;
    }


    public function calculate(int $id)
    {
        $scheduledJob = $this->scheduledMaintenanceJobsById->get($id);
        $result = new ScheduledMaintenanceJobResult();
        if ($scheduledJob instanceof ScheduledMaintenanceJob) {

            $maintenanceJob = $scheduledJob->maintenanceJob;
            $result
                ->setCars($scheduledJob->cars)
                ->setEngineerName($scheduledJob->engineer->name)
                ->setMaintenanceName($maintenanceJob->name)
                ->setMaintenanceType($maintenanceJob->type)
                ->setVatPercentage($this->vatPercentage->get($scheduledJob->vat))
                ->setMaintenanceStart(Carbon::createFromFormat('Y-m-d H:i', $scheduledJob->timeSlot->start));


            $sparePartDynamicPriceTotalExclVat = 0;

            $maintenanceJob->spareParts->each(
                function ($sparePart)
                use (&$sparePartDynamicPriceTotalExclVat) {
                    $dynamicPrice = $this->dynamicPrice->get($sparePart);
                    $sparePartDynamicPriceTotalExclVat += $dynamicPrice;
                    $sparePart->dynamicPrice = $dynamicPrice;
                }
            );

            $result->setSpareParts($maintenanceJob->spareParts);

            $result
                ->setSparePartFixedPriceTotalExclVat(
                    array_sum($this->fixedPricesAsArray->get($maintenanceJob->spareParts))
                )
                ->setSparePartDynamicPriceTotalExclVat($sparePartDynamicPriceTotalExclVat)
                ->setServiceHourRate(
                    $this->hourRateByCarbon->get($maintenanceJob->hourRate, $result->getMaintenanceStart())
                )
                ->setCalculatedServiceHours($maintenanceJob->calculatedServiceHours)
                ->setActualServiceHours($scheduledJob->realWorkingHours)
                ->setServiceHoursTotalPriceExclVat(
                    floatval($result->getCalculatedServiceHours() * $result->getServiceHourRate())
                );

            //TotalCalculation
            $result->setSparePartPriceTotalVat($this->getVatOfPriceExcludingVat($result->getVatPercentage(), $result->getSparePartDynamicPriceTotalExclVat()));
            $result->setSparePartPriceTotalInclVat($this->getPriceWithVatIncluded($result->getVatPercentage(), $result->getSparePartDynamicPriceTotalExclVat()));

            $result->setServiceHoursTotalPriceVat($this->getVatOfPriceExcludingVat($result->getVatPercentage(), $result->getServiceHoursTotalPriceExclVat()));
            $result->setServiceHoursTotalPriceInclVat($this->getPriceWithVatIncluded($result->getVatPercentage(), $result->getServiceHoursTotalPriceExclVat()));


            $result->setSubTotalExclVat($result->getServiceHoursTotalPriceExclVat() + $result->getSparePartDynamicPriceTotalExclVat());
            $result->setSubTotalVat($this->getVatOfPriceExcludingVat($result->getVatPercentage(), $result->getSubTotalExclVat()));
            $result->setTotalInclVat($this->getPriceWithVatIncluded($result->getVatPercentage(), $result->getSubTotalExclVat()));

        }
        return $result;
    }

    /**
     * @param int $vat
     * @param float $priceExcludingVat
     * @return float|int
     */
    private function getPriceWithVatIncluded(int $vat, $priceExcludingVat)
    {
        $vatPrice = $this->getVatOfPriceExcludingVat($vat, $priceExcludingVat);
        return $vatPrice + $priceExcludingVat;
    }


    /**
     * @param int $vat
     * @param float $priceExcludingVat
     * @return float|int
     */
    private function getVatOfPriceExcludingVat(int $vat, $priceExcludingVat)
    {
        return ($vat / 100) * $priceExcludingVat;
    }
}
