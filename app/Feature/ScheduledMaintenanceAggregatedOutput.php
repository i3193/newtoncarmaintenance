<?php

namespace App\Feature;

use Illuminate\Database\Eloquent\Collection;

class ScheduledMaintenanceAggregatedOutput
{
    public function getCarsAsConsoleTableArray(Collection $cars): array
    {
        $carsTableResult = array();
        $cars->each(
            function ($car)
            use (&$carsTableResult) {
                $carsTableResult[] = [
                    $car->id,
                    $car->vin,
                    $car->licensePlate,
                    $car->carModel->brand->name,
                    $car->carModel->name,
                    $car->customer->name,
                    $car->customer->email,
                ];
            }
        );
        return $carsTableResult;
    }

    public function getSparePartsAsConsoleTableArray(Collection $spareParts): array
    {
        $sparePartTableResult = array();
        $spareParts->each(
            function ($sparePart)
            use (&$sparePartTableResult) {
                $brandNames = join(',', $sparePart->brands->pluck('name')->toArray());
                $carModelNames = join(',', $sparePart->carModels->pluck('name')->toArray());

                $sparePartTableResult[] = [
                    $sparePart->id,
                    $sparePart->name,
                    $sparePart->partNumber,
                    ($brandNames) ?? '',
                    ($carModelNames) ?? '',
                    $sparePart->fixedPrice,
                    $sparePart->dynamicPrice
                ];
            }
        );
        return $sparePartTableResult;
    }
}
