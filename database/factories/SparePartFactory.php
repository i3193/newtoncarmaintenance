<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SparePartFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->realText(50),
            'ean' => $this->faker->numerify('#################'),
            'partNumber' => $this->faker->bothify('???####?#?##?#?#'),
            'fixedPrice' => $this->faker->randomFloat(2, 0, 1200)
        ];
    }
}
