<?php

namespace Database\Factories;

use App\Models\CarModel;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * vin = Vehicle Identification Number
     * @return array
     */
    public function definition()
    {
        return [
            'vin' => $this->faker->bothify('???#?#???#####??'),
            'licensePlate' => $this->faker->bothify('##-??-##'),
            'customer_id' => Customer::all()->random()->id,
            'car_model_id' => CarModel::all()->random()->id,
        ];
    }
}
