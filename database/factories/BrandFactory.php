<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BrandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'priceApiUrl' => 'http://newtoncarmaintenance.test/api/v1/price',
            'priceApiKey' =>$this->faker->sha256()
        ];
    }
}
