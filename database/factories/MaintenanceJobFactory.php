<?php

namespace Database\Factories;

use App\Models\HourRates;
use Illuminate\Database\Eloquent\Factories\Factory;

class MaintenanceJobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->realText(100),
            'type' => $this->faker->randomElement([
                'Engine', 'Brakes', 'Transmission', 'Fluids', 'Interior', 'Filters', 'Misc'
            ]),
            'calculatedServiceHours' => $this->faker->randomDigitNotNull(),
            'hour_rate_id' => HourRates::all()->random()->id,
        ];
    }
}
