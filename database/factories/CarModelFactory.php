<?php

namespace Database\Factories;

use App\Models\Brand;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement([
                'Corvette',
                'Spider',
                'Allante', 'Courier', 'Hombre', 'Pampa', 'Sportvan',
                'Alliance', 'Cressida', 'Horizon', 'Panamera', 'Sprint',
                'Alpine', 'Crider', 'Hornet', 'Parisienne', 'Sprinter',
                'Altima', 'Crossfire', 'Hummer', 'Park Avenue', 'Spyder',
                'Amanti', 'Crosstrek', 'Hunter', 'Park Ward', 'Squire',
            ]),
            'brand_id' => Brand::all()->random()->id
        ];
    }
}
