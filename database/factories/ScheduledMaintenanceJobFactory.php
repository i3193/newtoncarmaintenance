<?php

namespace Database\Factories;

use App\Models\Engineer;
use App\Models\HourRates;
use App\Models\MaintenanceJob;
use App\Models\TimeSlot;
use App\Models\Vat;
use Illuminate\Database\Eloquent\Factories\Factory;

class ScheduledMaintenanceJobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'realWorkingHours' => $this->faker->randomFloat(2, 0, 12),
            'engineer_id' => Engineer::all()->random()->id,
            'time_slot_id' => TimeSlot::all()->random()->id,
            'maintenance_job_id' => MaintenanceJob::all()->random()->id,
            'vat_id' => Vat::all()->random()->id,
        ];
    }
}
