<?php

namespace Database\Seeders;

use App\Models\CarModel;
use App\Models\MaintenanceJob;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            HourRatesSeeder::class,
            VatSeeder::class,
            BrandSeeder::class,
            CarModelSeeder::class,
            CustomerSeeder::class,
            CarSeeder::class,
            SparePartSeeder::class,
            MaintenanceJobSeeder::class,
            EngineerSeeder::class,
            TimeSlotSeeder::class,
            ScheduledMaintenanceJobSeeder::class
        ]);
    }
}
