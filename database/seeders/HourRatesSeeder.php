<?php

namespace Database\Seeders;

use App\Models\HourRates;
use Illuminate\Database\Seeder;

class HourRatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([75,80,85] as $hourRate){
            HourRates::factory([
                'regular' => $hourRate,
                'weekend' => ($hourRate * 1.5),
            ])->create();
        }
    }
}
