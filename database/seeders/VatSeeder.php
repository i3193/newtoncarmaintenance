<?php

namespace Database\Seeders;

use App\Models\HourRates;
use App\Models\Vat;
use Illuminate\Database\Seeder;

class VatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new Vat(['percentage' => 21]))->saveQuietly();
    }
}
