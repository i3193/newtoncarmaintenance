<?php

namespace Database\Seeders;

use App\Models\CarModel;
use App\Models\MaintenanceJob;
use App\Models\SparePart;
use Illuminate\Database\Seeder;

class MaintenanceJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MaintenanceJob::factory()
            ->count(25)
            ->create();

        $maintenanceJobs = MaintenanceJob::all();
        /**
         * @var int $index
         * @var SparePart $sparePart
         */
        foreach ($maintenanceJobs as $index => $maintenanceJob) {
            $sparePartIds = SparePart::all()->shuffle()->take(random_int(0, 20))->pluck('id');
            $maintenanceJob->spareParts()->sync($sparePartIds);
            $maintenanceJob->saveQuietly();
        }
    }
}
