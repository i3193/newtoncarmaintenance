<?php

namespace Database\Seeders;

use App\Models\Engineer;
use Illuminate\Database\Seeder;

class EngineerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Engineer::factory()
            ->count(10)
            ->create();
    }
}
