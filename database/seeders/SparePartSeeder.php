<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\CarModel;
use App\Models\SparePart;
use Illuminate\Database\Seeder;

class SparePartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carModels = CarModel::all();
        SparePart::factory()
            ->count(100)
            ->afterCreating(function($sparePart) use ($carModels) {
                $takenModels = $carModels->shuffle()->take(random_int(1, 5));
                $modelIds = $takenModels->pluck('id');
                $brandIds = array_map(fn($takenModel) => $takenModel['brand_id'], $takenModels->toArray());

                $sparePart->brands()->sync($brandIds);
                $sparePart->carModels()->sync($modelIds);

                $sparePart->saveQuietly();
            })
            ->create();

        //No Specific brand related SpareParts
        SparePart::factory()
            ->count(25)
            ->create();
    }
}
