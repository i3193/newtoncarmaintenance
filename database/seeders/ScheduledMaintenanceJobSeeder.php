<?php

namespace Database\Seeders;

use App\Models\Car;
use App\Models\ScheduledMaintenanceJob;
use Illuminate\Database\Seeder;

class ScheduledMaintenanceJobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ScheduledMaintenanceJob::factory()
            ->count(100)
            ->afterCreating(function($scheduledMaintenanceJob) {
                $carIds = Car::all()->random(rand(1,3))->pluck('id');
                $scheduledMaintenanceJob->cars()->sync($carIds);
            })
            ->create();
    }
}
