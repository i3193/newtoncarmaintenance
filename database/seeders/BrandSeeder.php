<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ([
                     'bmw',
                     'mercedes',
                     'alfa romeo',
                     'peugeot',
                     'opel',
                 ] as $brandName)
        {
           Brand::factory()->create(['name' => $brandName]);
        }

    }
}
