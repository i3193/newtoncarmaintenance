<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Holidays;
use App\Models\TimeSlot;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Seeder;

class TimeSlotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dateTime = Carbon::now();
        $start = '08:00';
        $end = '18:00';
        $endOfTheYear = Carbon::create($dateTime->year, 12, 31);
        do{
            $dateTime = $dateTime->addDay();
            $startDateTime = $dateTime->clone()->setTimeFromTimeString($start);
            $endDateTime = $dateTime->clone()->setTimeFromTimeString($end);
            foreach ($this->generateTimeRange($startDateTime, $endDateTime) as $date) {
                $timeSlot = new TimeSlot([
                    'start' => $date,
                ]);
                $timeSlot->saveQuietly();
            }
        } while($dateTime < $endOfTheYear);
    }

    private function generateTimeRange(Carbon $startDateTime, Carbon $endDateTime)
    {
        $dates = [];
        for($date = $startDateTime->copy(); $date->lte($endDateTime); $date->addHour()) {
            $dates[] = $date->format('Y-m-d H:i');
        }
        return $dates;
    }
}
