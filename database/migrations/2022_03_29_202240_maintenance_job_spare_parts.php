<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MaintenanceJobSpareParts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenance_job_spare_parts', function (Blueprint $table) {
            $table->integer('maintenance_job_id')->unsigned()->index();
            $table->foreign('maintenance_job_id')->references('id')
                ->on('maintenance_jobs');
            $table->integer('spare_part_id')->unsigned()->index();
            $table->foreign('spare_part_id')->references('id')
                ->on('spare_parts');
            $table->primary(['maintenance_job_id', 'spare_part_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenance_job_spare_parts');
    }
}
