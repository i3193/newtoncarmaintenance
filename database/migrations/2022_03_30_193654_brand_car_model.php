<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BrandCarModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_car_model', function (Blueprint $table) {
            $table->integer('brand_id')->unsigned()->index();
            $table->foreign('brand_id')->references('id')
                ->on('brands')
                ->onDelete('cascade');
            $table->integer('car_model_id')->unsigned()->index();
            $table->foreign('car_model_id')->references('id')
                ->on('car_models');
            $table->primary(['brand_id', 'car_model_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_car_model');
    }
}
