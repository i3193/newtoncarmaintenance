<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SparePartsBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spare_part_brands', function (Blueprint $table) {
            $table->integer('spare_part_id')->unsigned()->index();
            $table->foreign('spare_part_id')->references('id')
                ->on('spare_parts');
            $table->integer('brand_id')->unsigned()->index();
            $table->foreign('brand_id')->references('id')
                ->on('brands');
            $table->primary(['spare_part_id', 'brand_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spare_part_brands');
    }
}
