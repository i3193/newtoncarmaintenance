<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CarModelCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_model_cars', function (Blueprint $table) {
            $table->integer('car_model_id')->unsigned()->index();
            $table->foreign('car_model_id')->references('id')
                ->on('car_models');
            $table->integer('car_id')->unsigned()->index();
            $table->foreign('car_id')->references('id')
                ->on('cars');
            $table->primary(['car_model_id', 'car_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_model_cars');
    }
}
