<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('vin');
            $table->string('licensePlate');
            $table->integer('customer_id')->unsigned()->index();
            $table->foreign('customer_id')->references('id')
                ->on('customers');
            $table->integer('car_model_id')->unsigned()->index();
            $table->foreign('car_model_id')->references('id')
                ->on('car_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
