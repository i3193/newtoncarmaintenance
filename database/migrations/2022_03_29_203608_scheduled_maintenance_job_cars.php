<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ScheduledMaintenanceJobCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_maintenance_job_cars', function (Blueprint $table) {
            $table->integer('scheduled_maintenance_job_id')->unsigned()->index();
            $table->foreign('scheduled_maintenance_job_id')->references('id')
                ->on('scheduled_maintenance_jobs');
            $table->integer('car_id')->unsigned()->index();
            $table->foreign('car_id')->references('id')
                ->on('cars');
            $table->primary(['scheduled_maintenance_job_id', 'car_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spare_part_car_models');
    }
}
