<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SparePartsCarModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spare_part_car_models', function (Blueprint $table) {
            $table->integer('spare_part_id')->unsigned()->index();
            $table->foreign('spare_part_id')->references('id')
                ->on('spare_parts');
            $table->integer('car_model_id')->unsigned()->index();
            $table->foreign('car_model_id')->references('id')
                ->on('car_models');
            $table->primary(['spare_part_id', 'car_model_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spare_part_car_models');
    }
}
