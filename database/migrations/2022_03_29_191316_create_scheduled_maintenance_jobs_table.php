<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduledMaintenanceJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_maintenance_jobs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->float('realWorkingHours')->nullable();
            $table->integer('engineer_id')->unsigned()->index();
            $table->foreign('engineer_id')->references('id')
                ->on('engineers');
            $table->integer('time_slot_id')->unsigned()->index();
            $table->foreign('time_slot_id')->references('id')
                ->on('time_slots');
            $table->integer('maintenance_job_id')->unsigned()->index();
            $table->foreign('maintenance_job_id')->references('id')
                ->on('maintenance_jobs');
            $table->integer('vat_id')->unsigned()->index();
            $table->foreign('vat_id')->references('id')
                ->on('vats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_maintenance_jobs');
    }
}
